/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  ListViewDataSource,
  Image
} from 'react-native';

// 寫在程式中的 ListItem 資料
// 用陣列與 Map(key:value) 來製作
// 此為固定寫下的方式
const listItemData = [
    // 1
    {
        'name': {
            'first': '趙',
            'last': '大'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/4.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/4.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/4.jpg"
        }
    },

    // 2
    {
        'name': {
            'first': '錢',
            'last': '中'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/47.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/47.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/47.jpg"
        },
    },

    // 3
    {
        'name': {
            'first': '孫',
            'last': '微'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/13.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/13.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/13.jpg"
        },
    },

    // 4
    {
        'name': {
            'first': '李',
            'last': '老'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/22.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/22.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/22.jpg"
        },
    },

    // 5
    {
        'name': {
            'first': '周',
            'last': '天'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/53.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/53.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/53.jpg"
        },
    },

    // 6
    {
        'name': {
            'first': '吳',
            'last': '地'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/19.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/19.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/19.jpg"
        },
    },

    // 7
    {
        'name': {
            'first': '鄭',
            'last': '國'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/30.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/30.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/30.jpg"
        },
    },

    // 8
    {
        'name': {
            'first': '王',
            'last': '惟'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/95.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/95.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/95.jpg"
        },
    },

    // 9
    {
        'name': {
            'first': '黃',
            'last': '嚴'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/36.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/36.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/36.jpg"
        },
    },

    // 10
    {
        'name': {
            'first': '秦',
            'last': '功'
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/37.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/37.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/37.jpg"
        },
    },
];

export default class ListViewDemo extends Component {

    constructor(props) {
        super(props);

        this.addListItemData();

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            // dataSource: ds.cloneWithRows(['row 1', 'row 2', 'ABE', 'EDFE0', 'FRDEW','asdff','qwsdef', 'asdee']),
            dataSource: ds.cloneWithRows(listItemData),
            // dataSource: ds.cloneWithRows(this.addListItemData()),
      };
    }

    // 利用動態的方式，將要加入的資料，於執行時才加入
    addListItemData() {

        let listItemData = [];

        let first = new Map();
        let last = new Map();
        let name = new Map();

        let large = new Map();
        let picture = new Map();

        let person = new Map();

        // 1
        first.set('first', '趙');
        console.log(first);

        last.set('last', '大');
        name.set('name', first);
        name.set('name', last);

        large.set('large', 'https://randomuser.me/api/portraits/men/4.jpg');
        picture.set('picture', large);

        person.set('1', name);

        //
        listItemData.push(person);

        console.log(listItemData);

        return listItemData;
    }    

    renderRow(rowData) {
    
    // <View><Text>{rowData}</Text></View>
        return(
            <View style={styles.listitem}>

                <Image source={{uri: rowData.picture.large}} style={{width: 100, height: 100, borderRadius:50}} />
                <Text>
                    {rowData.name.first + ' ' + rowData.name.last}                    
                </Text>

            </View>
        );
    }

    render() {
        return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={(rowData) => this.renderRow(rowData)}
            />
        );
    }
}

/*
            <View style={styles.container}>


              style={styles.listview}

                <Image source={{ uri: props.picture.large}} style={styles.photo} />

              renderRow={(rowData) => this.renderRow(rowData)}

              renderRow={(rowData) => <View><Text>{rowData}</Text></View>}
*/


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listview: {
        flex: 1,
        marginTop: 20,
    },
    listitem: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 100,
    },
});

AppRegistry.registerComponent('ListViewDemo', () => ListViewDemo);
